﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ExplosiveForceRadius : MonoBehaviour
{
    public float radius = 100F;
    public float power = 10000F;
    public float upwardForce = 3f;

    void Update()
    {
        Vector3 explosionPos = transform.parent.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius,1<<0, QueryTriggerInteraction.Ignore);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
               rb.AddExplosionForce(power, explosionPos, radius,upwardForce,ForceMode.Impulse);
        }
    }
}
