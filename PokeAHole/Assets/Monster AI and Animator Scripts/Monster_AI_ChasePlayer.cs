﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Monster_AI_ChasePlayer : StateMachineBehaviour
{
    public float chaseTimer;
    public float maxChaseTime = 5f;
    Scr_EnemyVision enemyVision;
    NavMeshAgent nav;

    public float killDistance = 4f;




    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyVision = animator.GetComponent<Scr_EnemyVision>();
        nav = animator.GetComponent<NavMeshAgent>();

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {


        if (enemyVision.playerNearby == false)
        {
            //if player escapes
            chaseTimer += Time.deltaTime; 

            if(chaseTimer >= maxChaseTime)
            {
                animator.SetBool("playerSeen", false);
            }

        } else
        {
            //reset timer
            chaseTimer = 0;
            
            //chase player
            nav.SetDestination(enemyVision.player.transform.position + new Vector3(0,5,0));

            // if the distance is less than a certain threshold, do the lose state - call kill player. 
            if(Vector3.Distance(nav.transform.position, enemyVision.player.transform.position) < 4f)
            {
                Debug.Log("player Dead");
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        chaseTimer = 0; 
    }
}
