﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Video;

public class Monster_AI_MoveToNoisePoint : StateMachineBehaviour
{
    GameObject monsterModel;

    Scr_EnemyVision enemyVision;
    NavMeshAgent nav;

    public float timer;
    public float maxSearchTime;

    public bool startTimer;

    public AI_Monster chaseSound;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //monster model
        monsterModel = animator.transform.GetChild(0).gameObject;
        monsterModel.SetActive(true);


        enemyVision = animator.GetComponent<Scr_EnemyVision>();
        nav = animator.GetComponent<NavMeshAgent>();

        nav.SetDestination(GameManager.instance.lastPlayerLocation);

        chaseSound = animator.GetComponent<AI_Monster>();

        chaseSound.ChasePlayer();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(nav.remainingDistance < 0.5f)
        {
            startTimer = true;
        }

        if(startTimer)
        {
            
        }

        if (enemyVision.playerNearby)
        {
            animator.SetBool("playerSeen", true);
        }


        timer += Time.deltaTime;

        //countdown search time
        if (timer >= maxSearchTime)
        {
            monsterModel.SetActive(false);
            animator.SetBool("goToSleep", true);
        }


        // if the distance is less than a certain threshold, do the lose state - call kill player. 
        if (Vector3.Distance(nav.transform.position, GameManager.instance.player.transform.position) < 2f)
        {

            GameManager.instance.Die();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
        animator.SetBool("goToSleep", false);
    }
}
