﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyVision : MonoBehaviour
{
    [SerializeField] private float fieldOfViewAngle;

    public GameObject player;
    public bool alerted;
    public bool playerNearby;

    //methodology referenced from https://www.youtube.com/watch?v=mBGUY7EUxXQ
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Hit");
            //change the detection level to yellow for the light and eye (external script) 
            player = other.gameObject;
            playerNearby = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" && alerted == false)
        {
            //lost sight
            Debug.Log("Lost Player");
            player = null;
            playerNearby = false;
        }
    }

    private void Update()
    {
        if (playerNearby)
        {
            //get the direction between this and the player
            Vector3 direction = player.transform.position - this.transform.transform.position;

            //calculate angle 
            float angle = Vector3.Angle(direction, transform.forward);

            //check if it is within the field of view
            if (angle < fieldOfViewAngle * 0.5f)
            {
                //problem casting ray in apporiate direction 
                Ray ray = new Ray(this.transform.position, direction);

                Debug.DrawRay(this.transform.position, direction);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100f))
                {
                    if (hit.collider.tag == "Player")
                    {
                        alerted = true;
                        Debug.DrawLine(ray.origin, hit.point, Color.red);
                    }
                }
            }
        }
    }
}
