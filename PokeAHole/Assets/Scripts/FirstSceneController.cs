﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class FirstSceneController : MonoBehaviour
{
    [SerializeField]
    IntroDoor introDoor;

    // Start is called before the first frame update
    void Start()
    {
        PlayBossSequence();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public PlayableDirector bossSequence;
    [Button]
    void PlayBossSequence()
    {
        bossSequence.Play();
    }

    bool computerOn;
    [Button]
    public void TurnOnComputer()
    {
        computerOn = true;
        StartCoroutine(PLayComputerSequence());
    }

    [SerializeField]
    Text computerTerminal;
    bool firstInput = false;
    bool secondInput = false;
    bool thirdInput = false;

    [SerializeField]
    Animation turnOnComputer;

    bool buttonHit;
    public void ButtonHit()
    {
        if (!buttonHit)
        {
            StartCoroutine(ButtonHitTimer());
        }
    }

    IEnumerator ButtonHitTimer()
    {
        buttonHit = true;
        yield return null;
        buttonHit = false;
    }

    IEnumerator PLayComputerSequence()
    {
        //add computer startup ui
        turnOnComputer.Play();

        yield return new WaitForSeconds(turnOnComputer.clip.length);

        computerTerminal.text = "Commit all changes: (Y/N)";

        //wait for input to come
        while (!firstInput)
        {
            yield return null;
            if (buttonHit)
            {
                firstInput = true;
            }
        }

        computerTerminal.text = "Commit all changes: (Y/N)" + "\n" + "Commited all 349 changes" + "\n" + "Push all changes to master? (Y/N)";

        //wait for input to come
        while (!secondInput)
        {
            yield return null;
            if (buttonHit)
            {
                secondInput = true;
            }
        }

        computerTerminal.text = "Commit all changes: (Y/N)" + "\n" + "Commited all 349 changes" + "\n" + "Push all changes to master? (Y/N)" + "\n" + "Are you sure? (Y/N)";
        //wait for input to come
        while (!thirdInput)
        {
            yield return null;
            if (buttonHit)
            {
                thirdInput = true;
            }
        }

        yield return null;
        StartEmergency();
    }

    [SerializeField]
    Light officeLight;
    [SerializeField]
    Light officeLight2;
    [SerializeField]
    Animation turnOffMonitor;
    void StartEmergency()
    {
        //turn off lights
        officeLight.intensity = 0;
        officeLight2.intensity = 0;
        //turn off computer
        computerTerminal.enabled = false;
        turnOffMonitor.Play();

        introDoor.OpenDoor();

    }


}
