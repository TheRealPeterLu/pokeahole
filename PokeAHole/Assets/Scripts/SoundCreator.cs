﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider))]
public class SoundCreator : MonoBehaviour
{
    Rigidbody rb;
    AudioSource audioSource;

    public List<AudioClip> hitSound = new List<AudioClip>();
    public List<AudioClip> dragSound = new List<AudioClip>();

    public List<GameObject> collisions = new List<GameObject>();

    bool hasStarted;
    IEnumerator DisableSoundOnStart()
    {
        yield return new WaitForSeconds(5);
        hasStarted = true;
    }

    private void Awake()
    {
        rb = GetComponentInChildren<Rigidbody>();
        audioSource = GetComponentInChildren<AudioSource>();
    }

    private void Start()
    {
        StartCoroutine(DisableSoundOnStart());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!hasStarted)
            return;
        collisions.Add(collision.gameObject);
        if (hitSound.Count == 0)
            return;
        SoundManager.instance.MakeNoise(GetNoiseStrength(rb.velocity.magnitude), transform);

        if(GetNoiseStrength(rb.velocity.magnitude) > 0.3f)
        AudioSource.PlayClipAtPoint
        (
            hitSound[Random.Range(0, hitSound.Count)],
            transform.position,
            Mathf.Pow(GetNoiseStrength(rb.velocity.magnitude), 2)
        );
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!hasStarted)
            return;
        if (collisions.Contains(collision.gameObject))
        {
            collisions.Remove(collision.gameObject);
        }
    }

    bool toggle = false;
    private void FixedUpdate()
    {
        if (!hasStarted)
            return;
        if (dragSound.Count == 0)
            return;

        if (collisions.Count > 0 && rb.velocity.magnitude > 0.01f)
        {
            if(!toggle)
            {
                audioSource.PlayOneShot
                (
                    dragSound[Random.Range(0, dragSound.Count)], 
                    GetNoiseStrength(rb.velocity.magnitude)
                );

                SoundManager.instance.MakeNoise(GetNoiseStrength(rb.velocity.magnitude), transform);

                toggle = true;
            }
        }
        else if(collisions.Count == 0 || rb.velocity.magnitude < 0.01f)
        {
            audioSource.Stop();
            toggle = false;
        }
    }

    float GetNoiseStrength(float velocity)
    {
        return velocity * 1.5f;
    }
}
