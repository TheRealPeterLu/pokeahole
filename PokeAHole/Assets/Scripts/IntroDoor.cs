﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class IntroDoor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        door.GetComponent<Rigidbody>().isKinematic = true;
    }

    [SerializeField]
    GameObject door;
    [Button]
    public void OpenDoor()
    {
        door.GetComponent<Rigidbody>().isKinematic = false;

        door.transform.DOLocalRotate(new Vector3(0,20,0), 6).SetEase(Ease.OutExpo);

        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //set y rotation to 20
}
