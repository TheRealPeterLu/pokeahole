﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(InputController))]
public class MovementController : MonoBehaviour
{
    [Header("References")]
    public InputController inputCtrl;
    public Camera playerCam;
    public Rigidbody playerRB;
    CharacterSoundManager charSound;

    [Header("WASD movement")]
    float currentSpeed;
    Vector2 currentDirection;
    public float maxSpeed = 1;
    public float crouchMaxSpeed=0.5f;
    float maxUseSpeed;
    public float timeToMaxSpeed= 1;
    public float timeToZeroSpeed =1;
    float accelerationRate;
    float decelerationRate;
    public float crouchLowerHeight;



    [Header("Rotation Stuff")]
    public Vector2 horizontalForwardVector; //( x, z)
    public Vector2 horizontalRightVector; //( x, z)
    public GameObject RotatingHand;
    public GameObject RotatingHandAndCam;
    Vector2 handRotation;
    //sensitivity of moving hand
    public float mouseSensitivity=1;
    //strength of lerp of moving hand
    public float handLerpSensitivity = 1;

    //speed of moving cam
    public float camSensitivity = 1;
    //strength of lerp of moving cam
    public float camLerpSensitivity =1;

    public float horizontalAngleLimit = 55; //+- hand y 
    public float verticalAngleLimit = 35; //+- hand x

    public float horizontalAngleToLerp = 35; //+- hand y 
    public float verticalAngleToLerp = 20; //+- hand x

    public float verticalCamTopLimit = -40; //top max
    public float verticalCamBotLimit = 50; //top min


    [Header("RightClick stuff")]

    public GameObject fingerPokeTransform;
    public GameObject pointingModel;
    public GameObject gunModel;
    public GameObject pewpewPrefab;

    AudioSource audioSource;

    private void Awake()
    {
        charSound = GetComponentInChildren<CharacterSoundManager>();

        Cursor.lockState = CursorLockMode.Locked;
        if (!inputCtrl)
        {
            inputCtrl = GetComponent<InputController>();
        }
        if (!playerCam)
        {
            playerCam = Camera.main;
        }
    }


    private void Start()
    {
        //calculate movement rates
        accelerationRate = maxSpeed / timeToMaxSpeed;
        decelerationRate = maxSpeed / timeToZeroSpeed;
    }



    // Update is called once per frame
    void Update()
    {

        DoMovement();
        CalculateFingerPoint();
        DoFingerPoke();



    }


    /// <summary>
    /// wasd movement
    /// </summary>
    public void DoMovement()
    {

        horizontalForwardVector = new Vector2(playerCam.transform.forward.x, playerCam.transform.forward.z);
        horizontalRightVector = new Vector2(playerCam.transform.right.x, playerCam.transform.right.z);

        //check for crouch;
        if (inputCtrl.crouch)
        {

            maxUseSpeed = crouchMaxSpeed;
            RotatingHandAndCam.transform.localPosition = new Vector3(0,crouchLowerHeight, 0);
        }
        else
        {
            maxUseSpeed = maxSpeed;
            RotatingHandAndCam.transform.localPosition = Vector3.zero;
        }

        //if theres movement input
        if (inputCtrl.forwardInput != 0 || inputCtrl.sideInput != 0)
        {
            if (charSound!=null)
            charSound.FootSteps(currentSpeed);

            //direction stuff
            currentDirection = (inputCtrl.forwardInput * horizontalForwardVector + inputCtrl.sideInput * horizontalRightVector);
            currentDirection.Normalize();


            //speed stuff

            if (currentSpeed < maxUseSpeed)
            {
                //calc speed during runtime cause ata wanted to test lol
                if (timeToMaxSpeed!=0)
                {
                    accelerationRate = maxUseSpeed / timeToMaxSpeed;
                }
                else
                {
                    accelerationRate = maxUseSpeed;
                    Debug.Log("cant divide by 0 lol");
                }
                
                currentSpeed += accelerationRate * Time.deltaTime;
            }

        }

        //no movement input
        else
        {
            if (charSound != null)
                charSound.StopFootSteps();

            currentDirection = Vector2.zero;
            if (currentSpeed > 0)
            {
                //calc speed during runtime cause ata wanted to test lol
                if (timeToZeroSpeed != 0)
                {
                    decelerationRate = maxUseSpeed / timeToZeroSpeed;
                }
                else
                {
                    decelerationRate = maxUseSpeed;
                    Debug.Log("cant divide by 0 lol");
                }
                currentSpeed -= decelerationRate * Time.deltaTime;
            }
        }

        currentSpeed = Mathf.Clamp(currentSpeed, 0, maxUseSpeed);

        //do the actual movement

        playerRB.MovePosition(playerRB.position + new Vector3(currentDirection.x * currentSpeed, 0, currentDirection.y * currentSpeed));

    }

    //calculates lerp value of cam movement
    Vector2 camLerpSlide=Vector2.zero;
    //calculates lerp value of hand movement;
    Vector2 handLerpSlide = Vector2.zero;

  /// <summary>
  /// Moving finger around screen
  /// </summary>
    public void CalculateFingerPoint()
    {

        //calculate the angle of the hand around player
        handRotation += new Vector2(mouseSensitivity *inputCtrl.mouseMove.x, mouseSensitivity * inputCtrl.mouseMove.y);


        //calc hand rotations
        handRotation.x = Mathf.Clamp(handRotation.x, -verticalAngleLimit, verticalAngleLimit);
        handRotation.y = Mathf.Clamp(handRotation.y, -horizontalAngleLimit, horizontalAngleLimit);


        //calc lerp hand rotations
        handLerpSlide.x = Mathf.Lerp(handLerpSlide.x, handRotation.x, handLerpSensitivity);
        handLerpSlide.y = Mathf.Lerp(handLerpSlide.y, handRotation.y, handLerpSensitivity);


        //sets rotation of the hand model around player
        RotatingHand.transform.localRotation = Quaternion.Euler(handLerpSlide);



        //calculates which way the camera pans
        Vector2 camSlide = Vector2.zero;

        if (handRotation.x < -verticalAngleToLerp)
        {
            camSlide.x -= camSensitivity * Time.deltaTime;
        }
        if (handRotation.x > verticalAngleToLerp)
        {
            camSlide.x += camSensitivity * Time.deltaTime;
        }
        if (handRotation.y < -horizontalAngleToLerp)
        {
            camSlide.y -= camSensitivity * Time.deltaTime;
        }
        if (handRotation.y > horizontalAngleToLerp)
        {
            camSlide.y += camSensitivity * Time.deltaTime;
        }

        //this line does the camera move lerp
        camLerpSlide.x = Mathf.Lerp(camLerpSlide.x, camSlide.x, camLerpSensitivity);
        camLerpSlide.y = Mathf.Lerp(camLerpSlide.y, camSlide.y, camLerpSensitivity);


        float clampedRotationX;
        //this fixed the angle cause it goes from 0 to 360 on the x axis cause unity angles are so dumb
        if (RotatingHandAndCam.transform.localEulerAngles.x > 180)
        {
            clampedRotationX = Mathf.Clamp( camLerpSlide.x + RotatingHandAndCam.transform.localEulerAngles.x-360,  verticalCamTopLimit, verticalCamBotLimit);
        }
        else
        {
            clampedRotationX = Mathf.Clamp(camLerpSlide.x + RotatingHandAndCam.transform.localEulerAngles.x, verticalCamTopLimit, verticalCamBotLimit);
        }
          
        //no if statement for Y cause unity angles are so dumb
        float clampedRotationY = camLerpSlide.y + RotatingHandAndCam.transform.localEulerAngles.y;



        //rotates cam+hand around player
        RotatingHandAndCam.transform.localEulerAngles = new Vector3(clampedRotationX , clampedRotationY,  0f);
         

    }
    [SerializeField]
    float pokeDistance = 0.3f;

    /// <summary>
    /// poke her
    /// </summary>
    public void DoFingerPoke()
    {
        if (inputCtrl.rightClick)
        {
          //  gunModel.SetActive(true);
           // pointingModel.SetActive(false);
            if (inputCtrl.leftClick)
            {
                //pewpew

               GameObject bullet = Instantiate(pewpewPrefab, this.transform.position, Quaternion.identity);
               // bullet.GetComponent<MoveFast>().direction =RotatingHandAndCam.transform.forward;
            }

        }
        else
        {
           // gunModel.SetActive(false);
           // pointingModel.SetActive(true);

            if (inputCtrl.leftClick)
            {
                fingerPokeTransform.transform.localPosition = new Vector3(0, 0, Mathf.Lerp(fingerPokeTransform.transform.localPosition.z, pokeDistance, 0.5f));
                //point
            }
            else
            {
                fingerPokeTransform.transform.localPosition = new Vector3(0, 0, Mathf.Lerp(fingerPokeTransform.transform.localPosition.z, 0f, 0.5f));
            }
        }

     

      

    }
}
