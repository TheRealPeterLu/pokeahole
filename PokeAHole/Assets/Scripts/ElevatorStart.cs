﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorStart : MonoBehaviour
{
   public bool ID;
   public bool drink;
   public bool files;
    public bool elevatorOn;

    public AudioSource doorOpenSound;

    void Update()
    {
        if (drink && files && ID && !elevatorOn)
        {
            turnOnElevator();
        }
    }

    public void turnOnID()
    {

        ID = true;
    }

    public void turnOnfiles()
    {

        files = true;
    }
    public void turnOndrink()
    {

        drink = true;
    }

    public GameObject doorToOpen;

    public void turnOnElevator()
    {

        AudioSource.PlayClipAtPoint(doorOpenSound.clip, transform.position);
        //do stuff here
        elevatorOn = true;
        doorToOpen.SetActive(false);
    }


}
