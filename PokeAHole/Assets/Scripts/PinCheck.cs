﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinCheck : MonoBehaviour
{


   
    public enum Sequence { Red, Green, Blue };
    public Sequence[] checkForThis = new Sequence[5];
    public Sequence[] currentSequence = new Sequence[5];

    public int lastPress;
    public float pressTimer;
    public bool unlocked;

 

  
    /// <summary>
    /// red 0, green 1, blue 2
    /// </summary>
    /// <param name="_buttonNum"></param>
    public void AddButton(int _buttonNum)
    {
        if (_buttonNum != lastPress)
       {
            lastPress = _buttonNum;
            //shift stuff back
            for (int i = checkForThis.Length - 1; i >0; i--)
            {
                currentSequence[i ] = currentSequence[i-1];
            }

            currentSequence[0] = (Sequence)_buttonNum;

            StartCoroutine(ResetTimer());

            CheckForUnlock();
        }
       
    }

    public void CheckForUnlock()
    {
        for (int i = 0; i < checkForThis.Length; i++)
        {
            if (currentSequence[i] != checkForThis[i])
            {
                return;
            }
           
        }

        unlocked = true;
    }

    IEnumerator ResetTimer()
    {

        yield return new WaitForSeconds(0.5f);
        lastPress = 3;
        yield return null;
    }
}
