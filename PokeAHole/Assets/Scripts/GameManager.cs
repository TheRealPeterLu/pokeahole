﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public float noiseThreshold;
    public AI_Monster monster;

    public bool monsterStartChasingPlayer;
    public Vector3 lastPlayerLocation;

    public GameObject player;


    public Image black;

    public VideoPlayer youDied;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        instructionsUI.anchoredPosition = new Vector2(500, instructionsUI.anchoredPosition.y);
    }

    public void MonsterStartChasing()
    {
        monsterStartChasingPlayer = true;
        lastPlayerLocation = player.transform.position; 

        //will stuff
       // monster.ChasePlayer();
    }

    [Button]
    void TestInstruction()
    {
        SendInstructions("Find the power switch and turn off the power");
    }

    [SerializeField]
    Text instructionsText;
    [SerializeField]
    RectTransform instructionsUI;
    public void SendInstructions(string instructions)
    {
        //change text and animate in
        instructionsText.text = instructions;
        StartCoroutine(AnimateInstructionsUI());
    }

    float instructionShowTime = 7;
    IEnumerator AnimateInstructionsUI()
    {
        //animate in
        instructionsUI.DOAnchorPosX(0, 1).SetEase(Ease.OutExpo);
        yield return new WaitForSeconds(instructionShowTime);
        //animate out
        instructionsUI.DOAnchorPosX(500, 1).SetEase(Ease.OutExpo);
    }

    public void Collect(string text)
    {
        instructionsUI.GetComponent<Text>().text = text;
        StartCoroutine(AnimateInstructionsUI());
    }

    public void PowerDeactivateButton()
    {
        //play when breaker is pulled
        //turn off electricity
    }

    public void BeatGame()
    {
        StartCoroutine(Win());
    }

    public void Die()
    {
        Destroy(monster.gameObject);
        black.gameObject.SetActive(true);
        black.DOFade(1, 0);
        StartCoroutine(playOnDelay());

    }

    IEnumerator playOnDelay()
    {
        instance.player.GetComponentInChildren<AudioListener>().enabled = false;
        yield return new WaitForSeconds(2);
       
        player.gameObject.GetComponentInChildren<VideoPlayer>().Play();

        yield return new WaitForSeconds(0.5f);
        black.gameObject.SetActive(false);

        yield return new WaitForSeconds(15);
        SceneManager.LoadScene(2);


    }

    IEnumerator Win()
    {

        black.gameObject.SetActive(true);
        black.color = new Color(black.color.r, black.color.g, black.color.b, 0);


        black.DOFade(1, 3);
        

        yield return new WaitForSeconds(3);
        instance.player.GetComponentInChildren<AudioListener>().enabled = false;
        youDied.Play();

        yield return new WaitForSeconds(0.5f);
        black.gameObject.SetActive(false);
    }


}
