﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    
    void Update()
    {
        
    }

    /// <summary>
    /// good value is 0.15f and 0.4f
    /// </summary>
    /// <param name="_duration"></param>
    /// <param name="_magnitude"></param>
    /// <returns></returns>
    public IEnumerator Shake(float _duration, float _magnitude)
    {

        Vector3 originalPos = transform.localPosition;
        float elapsed = 0f;

        while (elapsed < _duration)
        {
            float x = Random.Range(-1f, 1) * _magnitude;
            float y = Random.Range(-1f, 1) * _magnitude;

            transform.localPosition = new Vector3(x, y, originalPos.z);

            elapsed += Time.deltaTime;

                yield return null;
        }

        transform.localPosition = originalPos;
    }
}
