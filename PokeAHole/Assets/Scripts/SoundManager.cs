﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Tell game manager how much noise has been made
    /// </summary>
    /// <param name="noiseStrength">should range from 0 - 1</param>
    public void MakeNoise(float noiseStrength, Transform soundCreator)
    {
        if(noiseStrength >= 1f)
        {
            noiseStrength = 1f;
        }
        else if(noiseStrength <= 0)
        {
            noiseStrength = 0f;
        }

        NoiseHandler(noiseStrength);

        print("Makes noise: " + noiseStrength);
    }


    public void NoiseHandler(float noiseStrength)
    {
        if(noiseStrength >= GameManager.instance.noiseThreshold)
        {
            GameManager.instance.MonsterStartChasing();
        }
    }
}
