﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class PhoneLogic : MonoBehaviour
{
    public UnityEvent OnButtonActivate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SendUpdateToUI()
    {
    }
    void OnTriggerEnter(Collider other)
    {
        OnButtonActivate.Invoke();
        GameManager.instance.SendInstructions("Get his ID Card, Red Binder and a Drink");
    }
}
