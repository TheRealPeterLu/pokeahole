﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_Monster : MonoBehaviour
{

    public InputController player;

    NavMeshAgent nav;

    public AudioSource spookSound;
    public AudioSource chaseSound;
    bool activated;

    private void Awake()
    {
       
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Transform GetPlayerPosition()
    {
        return player.transform;
    }

    void Spawn()
    {
        spookSound.Play();
        activated = true;
    }

    public void ChasePlayer()
    {
        if (!chaseSound.isPlaying)
        {
            //  StartCoroutine(Chasing());

            spookSound.Play();
    chaseSound.Play();
        }

        /*
        if (activated)
        {

            
        }
        else
        {
            Spawn();
        }
        */
    }

    public void StopChase()
    {
        if (chaseSound.isPlaying)
        {
            chaseSound.Stop();
        }
    }

    IEnumerator Chasing()
    {
        spookSound.Play();
        chaseSound.volume = 0;
        chaseSound.Play();

        while (true)
        {
            chaseSound.volume += 0.003f;

            if (chaseSound.volume >= 1)
            {
                chaseSound.volume = 1;
            }

            yield return null;

            //IF MONSTER TOUCHES PLAYER

        // END GAME

        // RETURN
        }
    }
}
