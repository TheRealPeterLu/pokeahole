﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputController : MonoBehaviour
{

    public float forwardInput;
    public float sideInput;
    public bool leftClick;
    public bool rightClick;
    public Vector2 mouseMove;
    public bool crouch;

    public bool escape;
   
    void Update()
    {
        if (!escape)
        {


            //leftclicks
            if (Input.GetMouseButton(0))
            {
                leftClick = true;

            }
            else
            {
                leftClick = false;
            }

            //rightclicks
            if (Input.GetMouseButton(1))
            {
                rightClick = true;

            }
            else
            {
                rightClick = false;
            }

            forwardInput = 0;

            if (Input.GetKey(KeyCode.W))
            {
                forwardInput++;
            }
            if (Input.GetKey(KeyCode.S))
            {
                forwardInput--;

            }


            sideInput = 0;

            if (Input.GetKey(KeyCode.A))
            {
                sideInput--;
            }
            if (Input.GetKey(KeyCode.D))
            {
                sideInput++;

            }

            mouseMove = new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));


            if (Input.GetKey(KeyCode.LeftControl))
            {
                crouch = true;
            }
            else
            {
                crouch = false;
            }

        }
        else
        {
            sideInput = 0;
            forwardInput = 0;
            mouseMove = Vector2.zero;
            leftClick = false;
            rightClick = false;
            
        }
            



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escape = !escape;
        }


        //Input.GetAxis("Vertical") ;
       //sideInput =Input.GetAxis("Horizontal") ;
    }

  

}
