﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSoundManager : MonoBehaviour
{
    AudioSource footsteps;
    float originalFootstepPitch;

    float maxVolume;
    float maxPitch;
    float minPitch;

    private void Awake()
    {
        footsteps = GetComponent<AudioSource>();
    }

    public void Start()
    {
        maxVolume = footsteps.volume;
        maxPitch = footsteps.pitch;
        minPitch = footsteps.pitch * 0.7f;
    }

    public void FootSteps(float speed)
    {
            footsteps.pitch = speed * 20 * maxPitch;
        
            footsteps.volume = speed * 20 * maxVolume;

        if (footsteps.pitch<= minPitch)
        {
            footsteps.pitch = minPitch;
        }


        if (!footsteps.isPlaying)
        {

            footsteps.Play();
        }
    }

    public void StopFootSteps()
    {
        if (footsteps.isPlaying)
            footsteps.Pause();
    }

    public void CrouchFootSteps()
    {
        if (!footsteps.isPlaying)
        {
            footsteps.pitch = originalFootstepPitch* 0.5f;
            footsteps.Play();
        }
    }
}
