﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkDrop : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other) {
        GetComponent<CollectItem>().turnOndrink();
        GetComponentInChildren<AudioSource>().Play();

        GameManager.instance.Collect("BOTTLE COLLECTED");
    }}
